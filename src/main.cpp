#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include "config.h"

DynamicJsonDocument Input(2048);
WiFiClient espClient;
PubSubClient client(espClient);

String msg = "";
unsigned long last_millis = 0;

void txCmd(uint8_t cmd)
{
  for (uint8_t k = 0; k < 15; k++)
  {
    //Serial.println(k);
    for (uint8_t i = 8; i > 0; i--)
    {
      if (cmd & (1 << (i-1)))
      {
        //Serial.print("1");
        digitalWrite(TXRF, HIGH);
        delayMicroseconds(3 * PERIOD);
        digitalWrite(TXRF, LOW);
        delayMicroseconds(PERIOD);
      }
      else
      {
        //Serial.print("0");
        digitalWrite(TXRF, HIGH);
        delayMicroseconds(PERIOD);
        digitalWrite(TXRF, LOW);
        delayMicroseconds(3 * PERIOD);
      }
    }
    for (uint8_t i = 0; i < 9; i++)
    {
      digitalWrite(TXRF, HIGH);
      delayMicroseconds(2 * PERIOD);
      digitalWrite(TXRF, LOW);
      delayMicroseconds(2 * PERIOD);
    }
    //Serial.println();
    delay(5);
  }
}

void callback(String topic, byte *message, unsigned int length)
{
  String messageTemp;
  for (int i = 0; i < length; i++)
  {
    messageTemp += (char)message[i];
  }
  Serial.print("MQTT receive : ");
  Serial.println(messageTemp);

  if (messageTemp.equals("OFF"))
  {
    txCmd(0xC0);
  }
  else if (messageTemp.equals("CMD1"))
  {
    txCmd(0xC1);
  }
  else if (messageTemp.equals("CMD2"))
  {
    txCmd(0xC2);
  }
  else if (messageTemp.equals("CMD3"))
  {
    txCmd(0xC3);
  }
  else if (messageTemp.equals("CMD4"))
  {
    txCmd(0xC4);
  }
  else if (messageTemp.equals("CMD5"))
  {
    txCmd(0xC5);
  }
  else if (messageTemp.equals("CMD6"))
  {
    txCmd(0xC6);
  }
  else if (messageTemp.equals("CMD7"))
  {
    txCmd(0xC7);
  }
  else if (messageTemp.equals("CMD8"))
  {
    txCmd(0xC8);
  }
  else if (messageTemp.equals("CMD9"))
  {
    txCmd(0xC9);
  }
  else if (messageTemp.equals("CMD10"))
  {
    txCmd(0xCA);
  }
}

void reconnect()
{
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");

    if (client.connect(HostName, mqttUser, mqttPassword))
    {
      Serial.println("connected");
      client.subscribe(topic_down);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      initNormalWifi();
    }
  }
}

void setup()
{
  Serial.begin(9600);

  pinMode(TXRF, OUTPUT);
  digitalWrite(TXRF, LOW);

  initNormalWifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void loop()
{
  if (WiFi.status() != WL_CONNECTED)
  {
    initNormalWifi();
  }
  if (!client.connected())
  {
    reconnect();
  }
  client.loop();
}

void initNormalWifi()
{
  int nbTry = 0;
  WiFi.mode(WIFI_STA);

  Serial.println("");
  WiFi.begin(SSID, PASSWORD);
  Serial.println("Try Wifi Connection");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.println(".");
    delay(500);
    nbTry++;
    if (nbTry > NBTRYWIFI)
    {
      while (1)
        ;
    }
  }
  Serial.println("ESP CONNECTED");
}
